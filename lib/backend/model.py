from sqlalchemy import ForeignKey
from sqlalchemy.engine import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, scoped_session, sessionmaker
from sqlalchemy.sql.schema import Column
from sqlalchemy.types import Boolean, Date, Integer, String

Base = declarative_base()


class Activity(Base):
    __tablename__ = 'activity'
    id = Column(Integer, primary_key=True, autoincrement=True)
    designation = Column(String, nullable=False)
    creation_date = Column(Date, nullable=False)
    last_update = Column(Date, nullable=True)
    joint_responsible = Column(String, nullable=True)
    app_name = Column(String, nullable=True)
    pursued_objectives = Column(String, nullable=False)

    persons = relationship(
        'Person', cascade='all, delete, delete-orphan', backref='activity'
    )
    categories = relationship(
        'Category', cascade='all, delete, delete-orphan', backref='activity'
    )

    external_links = relationship(
        'ExternalLink',
        cascade='all, delete, delete-orphan',
        backref='activity',
    )

    def get_categories(self):
        # fmt: off
        return {
            category.category_class: category
            for category in self.categories
        }
        # fmt: on

    def get_persons(self):
        result = {}
        for person in self.persons:
            if person.person_class in result:
                result[person.person_class].append(person)
            else:
                result[person.person_class] = [person]
        return result

    def get_external_links(self):
        # fmt: off
        return [
            link
            for link in self.external_links
        ]
        # fmt: on


class Person(Base):
    """ This table represents:
    concerned people
    recipients (intern recipients, extern company, subconstractor)
    """

    __tablename__ = 'person'
    id = Column(Integer, primary_key=True, autoincrement=True)
    person_class = Column(String, nullable=False)
    title = Column(String, nullable=False)

    activity_id = Column(Integer, ForeignKey('activity.id'))


class Category(Base):
    """ This table represents:
    datas collected
    retention duration
    datas transfer outside EU
    security measures
    """

    __tablename__ = 'category'
    id = Column(Integer, primary_key=True, autoincrement=True)
    category_class = Column(String, nullable=False)
    yes_no = Column(Boolean, nullable=False)
    comment = Column(String, nullable=False)

    activity_id = Column(Integer, ForeignKey('activity.id'))


class ExternalLink(Base):
    __tablename__ = 'externallink'
    id = Column(Integer, primary_key=True, autoincrement=True)
    link = Column(String, nullable=False)
    description = Column(String, nullable=True)

    activity_id = Column(Integer, ForeignKey('activity.id'))


class Db(object):
    def __init__(self):
        self.metadata = Base.metadata
        self.Session = sessionmaker()

    def init_app(self, app):
        self.app = app
        self.engine = create_engine(app.config['DB'])
        self.Session.configure(bind=self.engine)
        self.session = scoped_session(self.Session)

        @app.teardown_appcontext
        def teardown_appcontext(*args, **kwargs):
            db.session.remove()


db = Db()
