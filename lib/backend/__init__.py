import locale
import logging
import os

from datetime import date
from urllib.parse import urlparse

from flask import Flask

from .model import Activity, Category, Person, db


locale.setlocale(locale.LC_ALL, 'fr_FR')

app = Flask(__name__)
app.config.from_envvar('FLASK_CONFIG')
db.init_app(app)


@app.cli.command()
def drop_db():
    filename = urlparse(app.config['DB']).path
    if os.path.isfile(filename):
        os.remove(filename)


@app.cli.command()
def insert_data():
    db.session()
    db.session.add(
        Activity(
            id=1,
            designation='activity designation',
            creation_date=date(2018, 11, 13),
            last_update=date(2018, 11, 15),
            joint_responsible='other responsible',
            app_name='application name',
            pursued_objectives='to do smth',
        )
    )
    db.session.add(
        Person(
            id=1,
            person_class='concerned',
            title='concerned_title',
            activity_id=1,
        )
    )
    db.session.add(
        Person(
            id=2, person_class='intern', title='intern_title', activity_id=1
        )
    )
    db.session.add(
        Person(
            id=3, person_class='extern', title='extern_title', activity_id=1
        )
    )
    db.session.add(
        Person(id=4, person_class='sub', title='sub_title', activity_id=1)
    )
    db.session.add(
        Person(id=5, person_class='sub', title='sub_title2', activity_id=1)
    )

    db.session.add(
        Category(
            id=1,
            category_class='datas_retention_duration',
            yes_no=True,
            comment='retention_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=2,
            category_class='datas_transfer',
            yes_no=True,
            comment='datas_transfer_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=3,
            category_class='datas_collected-civil_state',
            yes_no=True,
            comment='datas_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=4,
            category_class='datas_collected-personnal_life',
            yes_no=True,
            comment='datas_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=5,
            category_class='datas_collected-professionnal_life',
            yes_no=True,
            comment='datas_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=6,
            category_class='datas_collected-financial',
            yes_no=True,
            comment='datas_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=7,
            category_class='datas_collected-data_connection',
            yes_no=True,
            comment='datas_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=8,
            category_class='datas_collected-data_location',
            yes_no=True,
            comment='datas_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=9,
            category_class='datas_collected-internet',
            yes_no=True,
            comment='datas_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=10,
            category_class='datas_collected-other',
            yes_no=True,
            comment='datas_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=11,
            category_class='datas_collected-sensible_datas',
            yes_no=True,
            comment='datas_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=12,
            category_class='security_measures-access_control',
            yes_no=True,
            comment='security_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=13,
            category_class='security_measures-traceability',
            yes_no=True,
            comment='security_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=14,
            category_class='security_measures-software_protection',
            yes_no=True,
            comment='security_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=15,
            category_class='security_measures-backup',
            yes_no=True,
            comment='security_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=16,
            category_class='security_measures-data_encryption',
            yes_no=True,
            comment='security_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=17,
            category_class='security_measures-subcontractors_control',
            yes_no=True,
            comment='security_comment',
            activity_id=1,
        )
    )
    db.session.add(
        Category(
            id=18,
            category_class='security_measures-other',
            yes_no=True,
            comment='security_comment',
            activity_id=1,
        )
    )
    db.session.commit()
    db.session.remove()


if app.debug:
    level = (
        logging.INFO
        if os.getenv('PYTHON_VERBOSE', os.getenv('VERBOSE'))
        else logging.WARNING
    )
    app.logger.setLevel(level)
    logging.getLogger('sqlalchemy').setLevel(level)
    logging.getLogger('sqlalchemy').handlers = logging.getLogger(
        'werkzeug'
    ).handlers
    logging.getLogger('sqlalchemy.orm').setLevel(logging.WARNING)
    logging.getLogger('unrest').setLevel(level)
    logging.getLogger('unrest').handlers = logging.getLogger(
        'werkzeug'
    ).handlers
    if level == logging.WARNING:
        logging.getLogger('werkzeug').setLevel(level)

from .routes import *  # noqa isort:skip
