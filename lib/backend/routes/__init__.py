from flask import redirect, render_template, request, url_for
from datetime import datetime

from .. import app
from ..model import Activity, Category, ExternalLink, Person, db


@app.route('/')
def register():
    activities = (
        db.session.query(Activity).order_by(Activity.designation).all()
    )
    return render_template('register.html', activities=activities)


@app.route('/activity/<activity_id>')
def activity(activity_id):
    activity = db.session.query(Activity).get(activity_id)
    categories = activity.get_categories()
    persons = activity.get_persons()
    external_links = activity.get_external_links()
    return render_template(
        'activity.html',
        activity=activity,
        categories=categories,
        persons=persons,
        external_links=external_links,
    )


@app.route('/activity/add', methods=['GET', 'POST'])
def add_activity():
    if request.method == 'POST':
        activity = Activity(
            designation=request.form['designation'],
            creation_date=datetime.utcnow().date(),
            last_update=datetime.utcnow().date(),
            joint_responsible=request.form['joint_responsible'],
            app_name=request.form['app_name'],
            pursued_objectives=request.form['pursued_objectives'],
        )
        db.session.add(activity)
        db.session.flush()
        for element in request.form:
            if element.startswith(('datas_', 'security_measures-')):
                db.session.add(
                    Category(
                        category_class=element,
                        yes_no=(request.form[element] is not ''),
                        comment=request.form[element],
                        activity_id=activity.id,
                    )
                )
        db.session.commit()
        return redirect(url_for('register'))

    return render_template('add_activity.html')


@app.route('/person/add/<activity_id>/<person_class>', methods=['GET', 'POST'])
def add_person(activity_id, person_class):
    activity = db.session.query(Activity).get(activity_id)

    if request.method == 'POST':
        activity.last_update = datetime.utcnow().date()
        db.session.add(
            Person(
                person_class=person_class,
                title=request.form[person_class],
                activity_id=activity_id,
            )
        )
        db.session.commit()
        return redirect(url_for('activity', activity_id=activity.id))

    labels = {
        'concerned': 'Catégories de personnes concernées',
        'intern': 'Destinataires internes',
        'extern': 'Organismes externes',
        'sub': 'Sous-traitants',
    }
    return render_template(
        'add_person.html',
        activity_id=activity_id,
        person_class=person_class,
        labels=labels,
    )


@app.route('/external_link/add/<activity_id>', methods=['GET', 'POST'])
def add_external_link(activity_id):
    activity = db.session.query(Activity).get(activity_id)

    if request.method == 'POST':
        activity.last_update = datetime.utcnow().date()
        db.session.add(
            ExternalLink(
                link=request.form['link'],
                description=request.form['description'],
                activity_id=activity_id,
            )
        )
        db.session.commit()
        return redirect(url_for('activity', activity_id=activity.id))

    return render_template('add_link.html', activity_id=activity_id)


@app.route('/activity/edit/<activity_id>', methods=['GET', 'POST'])
def edit_activity(activity_id):
    activity = db.session.query(Activity).get(activity_id)
    categories = activity.get_categories()

    if request.method == 'POST':
        activity.last_update = datetime.utcnow().date()
        for element in request.form:
            if element in [
                'designation',
                'joint_responsible',
                'app_name',
                'pursued_objectives',
            ]:
                setattr(activity, element, request.form[element])
            elif element.startswith(('datas_', 'security_measures-')):
                categories[element].comment = request.form[element]
                categories[element].yes_no = request.form[element] is not ''

        db.session.commit()
        return redirect(url_for('activity', activity_id=activity.id))

    return render_template(
        'edit_activity.html', activity=activity, categories=categories
    )


@app.route('/person/edit/<person_id>', methods=['GET', 'POST'])
def edit_person(person_id):
    person = db.session.query(Person).get(person_id)
    labels = {
        'concerned': 'Catégories de personnes concernées',
        'intern': 'Destinataires internes',
        'extern': 'Organismes externes',
        'sub': 'Sous-traitants',
    }

    if request.method == 'POST':
        person.activity.last_update = datetime.utcnow().date()
        person.title = request.form[person.person_class]
        db.session.commit()
        return redirect(url_for('activity', activity_id=person.activity.id))

    return render_template('edit_person.html', person=person, labels=labels)


@app.route('/external_link/edit/<external_link_id>', methods=['GET', 'POST'])
def edit_external_link(external_link_id):
    link = db.session.query(ExternalLink).get(external_link_id)

    if request.method == 'POST':
        link.activity.last_update = datetime.utcnow().date()
        link.link = request.form['link']
        link.description = request.form['description']
        db.session.commit()
        return redirect(url_for('activity', activity_id=link.activity.id))

    return render_template('edit_link.html', external_link=link)


@app.route('/activity/delete/<activity_id>', methods=['POST'])
def delete_activity(activity_id):
    activity = db.session.query(Activity).get(activity_id)
    db.session.delete(activity)
    db.session.commit()
    return redirect(url_for('register'))


@app.route('/person/delete/<person_id>', methods=['POST'])
def delete_person(person_id):
    person = db.session.query(Person).get(person_id)
    person.activity.last_update = datetime.utcnow().date()
    db.session.delete(person)
    db.session.commit()
    return redirect(url_for('activity', activity_id=person.activity_id))


@app.route('/external_link/delete/<external_link_id>', methods=['POST'])
def delete_external_link(external_link_id):
    link = db.session.query(ExternalLink).get(external_link_id)
    link.activity.last_update = datetime.utcnow().date()
    db.session.delete(link)
    db.session.commit()
    return redirect(url_for('activity', activity_id=link.activity_id))
