include MakeCitron.Makefile


upgrade-db:
	$(LOG)
	$(VENV)/bin/alembic upgrade head

insert-data:
	$(LOG)
	$(PIPENV) run flask insert-data

install-db: install-db-super ## install-db: Install apparatus database
	$(LOG)
	$(PIPENV) run flask drop-db
	$(MAKE) upgrade-db
	$(MAKE) insert-data

deploy-prod:
	$(LOG)
	@echo "Communicating with Junkrat..."
	@wget --no-verbose --content-on-error -O- --header="Content-Type:application/json" --post-data=$(subst $(newline),,$(JUNKRAT_PARAMETERS)) $(JUNKRAT) | tee $(JUNKRAT_RESPONSE)
	if [[ $$(tail -n1 $(JUNKRAT_RESPONSE)) != "Success" ]]; then exit 9; fi
	wget --no-verbose --content-on-error -O- $(URL_PROD)
