"""external link

Revision ID: c98e2d05293b
Revises: d06a6c2dc23f
Create Date: 2019-02-12 17:35:33.219855

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'c98e2d05293b'
down_revision = 'd06a6c2dc23f'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'externallink',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('link', sa.String(), nullable=False),
        sa.Column('activity_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['activity_id'], ['activity.id']),
        sa.PrimaryKeyConstraint('id'),
    )


def downgrade():
    op.drop_table('externallink')
