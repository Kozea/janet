"""alembic init

Revision ID: d06a6c2dc23f
Revises: 
Create Date: 2018-11-16 15:27:50.616459

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'd06a6c2dc23f'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'activity',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('designation', sa.String(), nullable=False),
        sa.Column('creation_date', sa.Date(), nullable=False),
        sa.Column('last_update', sa.Date(), nullable=True),
        sa.Column('joint_responsible', sa.String(), nullable=True),
        sa.Column('app_name', sa.String(), nullable=True),
        sa.Column('pursued_objectives', sa.String(), nullable=False),
        sa.PrimaryKeyConstraint('id'),
    )
    op.create_table(
        'category',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('category_class', sa.String(), nullable=False),
        sa.Column('yes_no', sa.Boolean(), nullable=False),
        sa.Column('comment', sa.String(), nullable=False),
        sa.Column('activity_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['activity_id'], ['activity.id']),
        sa.PrimaryKeyConstraint('id'),
    )
    op.create_table(
        'person',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('person_class', sa.String(), nullable=False),
        sa.Column('title', sa.String(), nullable=False),
        sa.Column('activity_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['activity_id'], ['activity.id']),
        sa.PrimaryKeyConstraint('id'),
    )


def downgrade():
    op.drop_table('person')
    op.drop_table('category')
    op.drop_table('activity')
