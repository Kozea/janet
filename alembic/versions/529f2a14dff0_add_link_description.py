"""add link description

Revision ID: 529f2a14dff0
Revises: c98e2d05293b
Create Date: 2019-02-13 11:09:50.809445

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '529f2a14dff0'
down_revision = 'c98e2d05293b'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        'externallink', sa.Column('description', sa.String(), nullable=True)
    )


def downgrade():
    op.drop_column('externallink', 'description')
